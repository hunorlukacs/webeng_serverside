@extends('layouts.main')

@section('content')

    <div class="container py-3">
      <h2>New book</h2>
      <form action="/projects" method="post">
        <!-- HTTP methods: get, post, put, delete -->
        @csrf
        
        <div class="form-group">
          <label for="name">Book title</label>
          <input value="{{ old('title', '') }}" type="text" class="form-control @error('name') is-invalid @enderror" name="title" id="title" placeholder="">
          @error('title')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>

        <div class="form-group">
          <label for="name">Author</label>
          <input value="{{ old('author', '') }}" type="text" class="form-control @error('name') is-invalid @enderror" name="author" id="author" placeholder="">
          @error('author')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>

        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3">{{ old('description', '') }}</textarea>
          @error('description')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>


        <div class="form-group">
          <button type="submit" class="btn btn-primary">Add new book</button>
        </div>

      </form>
    </div>
@endsection