<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookFormRequest;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index() {
        $books = Book::all();
        return view('books.list', ['books' => $books]);
    } // books/list.blade.php

    public function create() {
        return view('books.create'); // books/create.blade.php
    }

    public function store(BookFormRequest $request) {
        Book::create($request->validated());
        return redirect()->route('projects.index'); // GET
    }
}
